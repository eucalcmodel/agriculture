# Changelog

## v2.2
### Added

- LULUCF - converted lands
- LULUCF - lands remaining lands
- Forestry outputs
- Forest clearing emissions
- Carbon dynamics
- Bionergy capacity lever (dummies)
- Interface with minerals

### Changed
- Climate smart crop lever: variables added
- Land management lever: new variables added
- Climate smart forestry: new varibales added
- Forestry has been mooved in LULUCF

### Removed
- Bug with negative cellulosic crop demand
- Climate smart fisheries lever

###v2.1

### Intereface

- lus-clm updated
- tec-agr updated
- agr-lus updated

### Update 
- Forestry module 0.9 implemeted
- LULUCF module under implementation


###v2.0

### Intereface
- Agr-tpe updated
- Lus-tpe update
- lus-clm updated
- agr-emp updated
- agr-gtap added

### Update 
- model calibration (agr)
- grassland assessment updated (agr-lus)
- Activity calibration upgarded

###1.95

### Intereface
- Agr-tpe updated
- Lus-agr update
- lus-clm updated
- agr-emp updated

### Update 
- model calibration completed at 25%

##v1.9
### Update 
- all levers inputs have been updated 
- Livestock population problem solved
- Dummies between agr-lus-tpe has been replaced by model outputs (some dunmmies remain)

### Intereface
- Agr-tpe updated
- Lus-tpe update
- lus-clm updated
- agr-emp updated


##v1.6
### Update 
- Crop flows in kcal instead of tons
- Crop lever updated
- Land management lever updated
- Biomass hierarchy lever updated
- All the code is now in series and not in parralel
- Food group categories have been aggregated


##v1.5
### Update 
- Livestock flows in kcal instead of tons
- Livestock lever upgraded 
- Alternative Protein Source lever also upgraded 


## v1.4
### Lever removed from WP4 
- Biodiversity (now stand alone module)
- Water management (now stand alone module)
- Minerals mining (now stand alone module)

### Lever name update
- Forestry management to Climate Smart Forestry

### New-sub module
- Forest module draft added in land-use

### Interface update:
- Water management interaction updated 

## 1.4
### Added
Water output for the water module has been added to the agriculture node
Timber demand has been added as an input from industry to agriculture
### Changes
Update of transport input file
Vectors names based on the vector spreadsheet

## 0.9
- Initial version, integrating all modules